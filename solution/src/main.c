#include "../include/bmp.h"
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char **argv){
  if (argc != 4){
    fprintf(stderr, "Program can't start!\n");
    return 1;
  }
  // error handling should be here
  FILE *f1 = fopen(argv[1], "rb");
  if (f1 == NULL){
    fprintf(stderr, "File '%s' -> open error!\n", argv[1]);
    return 1;
	}

  FILE *f2 = fopen(argv[2], "wb");
  if (f2 == NULL){
    fclose(f1);
    fprintf(stderr, "File '%s' -> create error!\n", argv[2]);
  }
	
  long rotate_degrees = strtol(argv[3], NULL, 10);
  //printf("rotate_degrees = %d\n", rotate_degrees);
  struct image img = {.data=NULL};
  enum read_status rs = from_bmp(f1, &img);  
  if (rs != READ_OK){
    fprintf(stderr, "File '%s' -> read error!\n", argv[1]);
    fclose(f1);
    fclose(f2);
    free(img.data);
    return 1;
  }
  
  struct image img_result = {0,0,NULL};

  if(rotate_degrees == 0){
    enum write_status ws = to_bmp(f2, &img);
    if (ws != WRITE_OK){
      fprintf(stderr, "File '%s' -> write error!\n", argv[1]);
      fclose(f1);
      fclose(f2);
      free(img.data);
      return 1;
    }  
  }
  else if(rotate_degrees==90||rotate_degrees==-270){
    img_result = rotate(img);
  }
  else if(rotate_degrees==180||rotate_degrees==-180){    
    struct image tmp_image = rotate(img);
    img_result = rotate(tmp_image);
    free(tmp_image.data);
  }
  else{
    struct image tmp_image = rotate(img);
    struct image tmp_image2 = rotate(tmp_image);
    img_result = rotate(tmp_image2);
    free(tmp_image.data);
    free(tmp_image2.data);
  }

  enum write_status ws = to_bmp(f2, &img_result);
  if (ws != WRITE_OK){
    fprintf(stderr, "File '%s' -> write error!\n", argv[1]);
    free(img.data);
    free(img_result.data);
    fclose(f1);
    fclose(f2);
    return 1;
  }
	
  free(img.data);
  free(img_result.data);
  fclose(f1);
  fclose(f2);
  return 0;
}
