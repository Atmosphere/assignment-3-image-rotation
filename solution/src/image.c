#include "../include/image.h"
#include <stdlib.h>


/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate(struct image const source){
  struct image result_image;

  if (source.data == NULL) {
    result_image.height = 0;
    result_image.width = 0;
    result_image.data = NULL;
    return result_image;
  }

  struct pixel *p = (struct pixel *)malloc(sizeof(struct pixel) * source.width * source.height);

  if (p == NULL) {
    result_image.height = 0;
    result_image.width = 0;
    result_image.data = NULL;
    return result_image;
  }

  for (uint64_t row = 0; row < source.height; ++row){
    for (uint64_t col = 0; col < source.width; ++col){
      uint64_t row2 = col;
      uint64_t col2 = source.height - 1 - row;
      p[row2 * source.height + col2] = source.data[row * source.width + col];
    }
  }

  result_image.width = source.height;
  result_image.height = source.width;
  result_image.data = p;

  return result_image;
}
