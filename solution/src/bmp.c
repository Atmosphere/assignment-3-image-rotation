#include "../include/bmp.h"
#include <stdio.h>
#include <stdlib.h>

#define BMP_DETECT_MARK 0x4d42
#define HEADER_SIZE 0x28
#define TRUE_COLOR_BPP 0x18
#define DEFAULT_PPM 2835
#define BMP_LINE_MIN_GRANULARITY 4
#define DEFAULT_BUF_BYTE_SIZE 4

static uint8_t compute_padding(uint64_t width_pixels){
  if((width_pixels % BMP_LINE_MIN_GRANULARITY) == 0) return 0;
  else return ((uint8_t) (BMP_LINE_MIN_GRANULARITY - ((width_pixels * sizeof(struct pixel)) % BMP_LINE_MIN_GRANULARITY)));
}

enum read_status from_bmp(FILE *in, struct image *img){
  if (in == NULL || img == NULL) return READ_NULL_ARG;

  struct bmp_header hdr;
  uint64_t n_bytes = fread(&hdr, 1, sizeof(struct bmp_header), in);
  if (n_bytes != sizeof(struct bmp_header)){
    return READ_INVALID_HEADER;
  }
  if (hdr.bfType != BMP_DETECT_MARK || hdr.bOffBits != sizeof(struct bmp_header) || hdr.biBitCount != TRUE_COLOR_BPP){
    return READ_INVALID_HEADER;
  }

  img->width = hdr.biWidth;
  img->height = hdr.biHeight;

  // printf("width = %llu  height = %llu\n", img->width, img->height);
  free(img->data);
  img->data = (struct pixel *)malloc(sizeof(struct pixel) * img->width * img->height);
  // uint64_t bytes_per_line = sizeof (struct pixel) * img -> width;

  // uint64_t stub = 4 - (img->width * 3) % 4;
  uint8_t stub_bytes = compute_padding(img->width);

  uint8_t buf[DEFAULT_BUF_BYTE_SIZE];
  for (int64_t row = 0; row < img->height; ++row){
    // read next line
    // first line - bottom of image
    n_bytes = fread(img->data + (img->height - 1 - row) * img->width, 1, img->width * sizeof(struct pixel), in);
    if (n_bytes != img->width * sizeof(struct pixel)){
      return READ_INVALID_DATA;
    }
    // printf("row = %lld k = %lld\n", row, k);

    if (stub_bytes != 0){
      n_bytes = fread(buf, 1, stub_bytes, in);
      if(n_bytes!=stub_bytes){
        return READ_INVALID_DATA;
      }
    }
  }
  return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img){
  if (out == NULL || img == NULL) return WRITE_NULL_ARG;

  struct bmp_header hdr;
  // uint64_t helper_bytes = 4 - (img->width * 3) % 4;
  uint8_t stub_bytes = compute_padding(img->width);

  hdr.bfType = BMP_DETECT_MARK;
  hdr.bfileSize = sizeof(struct bmp_header) + img->height * (img->width * sizeof(struct pixel) + stub_bytes);
  hdr.bfReserved = 0;
  hdr.bOffBits = sizeof(struct bmp_header);
  hdr.biSize = HEADER_SIZE;
  hdr.biWidth = img->width;
  hdr.biHeight = img->height;
  hdr.biPlanes = 1;
  hdr.biBitCount = TRUE_COLOR_BPP;
  hdr.biCompression = 0;
  hdr.biSizeImage = img->height * (img->width * sizeof(struct pixel) + stub_bytes);
  hdr.biXPelsPerMeter = DEFAULT_PPM;
  hdr.biYPelsPerMeter = DEFAULT_PPM;
  hdr.biClrUsed = 0;
  hdr.biClrImportant = 0;
  uint64_t n_bytes = fwrite(&hdr, 1, sizeof(struct bmp_header), out);

  if (n_bytes != sizeof(struct bmp_header)){
    return WRITE_ERROR;
  }
  uint8_t byte_buf[DEFAULT_BUF_BYTE_SIZE] = {0};
  for (int64_t i = 0; i < img->height; ++i){
    // write current line of pixels
    n_bytes = fwrite(img->data + (img->height - 1 - i) * img->width, sizeof(struct pixel), img->width, out);
    if (n_bytes != img->width){
      return WRITE_ERROR;
    }
    if (stub_bytes != 0){
      n_bytes = fwrite(byte_buf, 1, stub_bytes, out);
      if (n_bytes != stub_bytes){
        return WRITE_ERROR;
      }
    }
  }
  
  return WRITE_OK;
}
